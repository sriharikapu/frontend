import { ethers } from 'ethers';
// import { web3 } from 'web3';
// const abi = require('../contracts-abi/ArrowdexEscrow.json');
// const { mul, pow } = require('../bn');
// const escrowTokens = require('../escrowTokens');
// const { GAS, GAS_PRICE } = require('./config');

// const decimalsForAmount = 18;

import abi from './abi'


class CHTContract {
  constructor() {

    const provider = new ethers.providers.Web3Provider(window.web3.currentProvider);
    const signer = provider.getSigner();
    this._contractAddress = "0x7ce87E62409b797FA7A338df631F52db358B56a3";
    this._contractInstance = new ethers.Contract(this._contractAddress, abi, signer);
  }

  get contractAddress() {
    return this._contractAddress;
  }

  get web3() {
    return this._web3;
  }

  formatAddress(address) {
    return this._web3.utils.asciiToHex(address).substring(0, 30);
  }

  // newHarvest(address grower, string memory ipfsHashNewHarvest)
  async newHarvest(growerAddress, ipfsHashNewHarvest) {
    try {

      let tx = await this._contractInstance.newHarvest(
        growerAddress,
        ipfsHashNewHarvest
      );
      let receipt = await tx.wait();
      let event = receipt.events.pop()
      console.log(event)
      const newId = ethers.utils.bigNumberify(event.data).toNumber()
      return newId;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  // function registerSample(uint256 cropId, string memory sampleJsonIpfsHash)
  async registerSample(cropId, ipfsHashSample) {
    try {

      let tx = await this._contractInstance.registerSample(
        cropId,
        ipfsHashSample
      );
      await tx.wait();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  async approveLaboratory(laboratoryAddress, cropId) {
    try {

      let tx = await this._contractInstance.approve(laboratoryAddress,cropId);
      await tx.wait();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
}
export default CHTContract;
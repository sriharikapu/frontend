import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home.vue';
import Grower from './views/Grower.vue';
import Lab from './views/Lab.vue';
import Gov from './views/Gov.vue';
import Validate from './views/Validate.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/grower',
      name: 'grower',
      component: Grower
    }, 
    {
      path: '/lab',
      name: 'lab',
      component: Lab
    },
    {
      path: '/validate',
      name: 'validate',
      component: Validate
    },
    {
      path: '/gov',
      name: 'gov',
      component: Gov
    } 

  ]
});

// router.beforeResolve(async (to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     try {
//       const user = await Auth.currentAuthenticatedUser();
//       if (!user) {
//         throw new Error('Empty user from cognito!');
//       }
//     } catch (e) {
//       console.log(e);
//       return next({ path: '/login' });
//     }
//   }
//   next();
// });

export default router;